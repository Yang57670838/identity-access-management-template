const AWS = require('aws-sdk')

// AWS.config.update({ region: 'ap-southeast-2'})
AWS.config.loadFromPath('./config.json')

const dynamodb = new AWS.DynamoDB()

dynamodb.listTables((err, data) => {
    if (err) {
        console.error(err)
    } else {
        console.log(data)
    }
})