const mongoose = require('mongoose')
const config = require('config')
const db = config.get('mongoDevURI')

const connectDB = async () => {
    try {
        await mongoose.connect(db)
        console.log('Mongodb Connected..')
    } catch(err) {
        console.error(err.message)
        //EXIT process
        process.exit(1)
    }
}

module.exports = connectDB