# Identity Access Management Template

a nodejs template app for managing identity access

## Build image

**dev**
docker build . -t yang/identify-app:v1 -target=dev

**production**
docker build . -t yang/identify-app:v1 -target=production

## Run Image

```
`docker run --rm --cpus=0.4 -m 400m --memory-reservation 250m --name identify-app -d -p 5000:5000 -v /home/node/identifyapp yang/identify-app:v1`
remove the anonymous volume and container after exit app
assume its in a T2 micro EC2 with 1 CPU and 1GB memoey only, so assign 0.4 CPU and 400MB RAM hard limit, 250MB RAM soft limit to this container
`localhost:5000`
```