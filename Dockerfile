# build image for dockerize this app
FROM node:10.16.3-alpine as base
WORKDIR /home/node/identifyapp
RUN chown -R node:node .
USER node
COPY package*.json ./
COPY . ./
EXPOSE 5000

FROM base as dev
ENV APPENV=dev
RUN npm install
CMD ["npm", "start"]

FROM base as production
ENV APPENV=production
RUN npm install --production
HEALTHCHECK --interval=5m --timeout=3s --retries=2 --start-period=15s \
    CMD node ./utility/healthcheck.js
CMD ["npm", "run", "server"]