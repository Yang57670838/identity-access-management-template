const cluster = require('cluster')

if (cluster.isMaster) {
    cluster.fork()
    cluster.fork()
} else {
    const express = require('express')
    const bodyParser = require('body-parser')

    // switch from MongoDB to DynamoDB
    // const connectDB = require('./db/db')
    // connectDB()

    // check work env form image
    const env = process.env.APPENV || 'dev'

    const app = express()
    app.use(bodyParser.json())


    const claims = {
        Role: "admin",
        UserID: "12345",
        Fullname: "Yang Liu",
        ExpireDateTime: "2020-01-01",
        Subject: "temporary-template-front-end" //who requires this JWT
    }

    app.get('/', (req, res) => res.send('API Running'))

    app.post('/login', (req, res) => {
        let body = req.body
        body.message = "test " + body.Fullname

        res.json(body)
    })

    



    const PORT = process.env.PORT || 5000

    app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
}